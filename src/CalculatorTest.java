import org.junit.jupiter.api.*;

public class CalculatorTest {

    LambdaCalculator lambdaCalculator = new LambdaCalculator();


    @Test
    @DisplayName("Возвращает результат сложения")
    void calculator_returnPlus() {
        Assertions.assertEquals(lambdaCalculator.calculate(2, 4, Integer::sum), 6);
    }

    @Test
    @DisplayName("Возвращает результат вычитания")
    void calculator_returnMinus() {
        Assertions.assertEquals(lambdaCalculator.calculate(4, 2, (a, b) -> a - b), 2);
    }

    @Test
    @DisplayName("Возвращает результат умножения")
    void calculator_returnMultiply() {
        Assertions.assertEquals(lambdaCalculator.calculate(4, 2, (a, b) -> a * b), 8);
    }

    @Test
    @DisplayName("Возвращает результат деления")
    void calculator_returnDivide() {
        Assertions.assertEquals(lambdaCalculator.calculate(4, 2, (a, b) -> a / b), 2);
    }
}
