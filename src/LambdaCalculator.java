
public class LambdaCalculator {
    public Integer calculate(Integer a, Integer b, Operation operation) {
        return operation.operation(a, b);
    }
}
