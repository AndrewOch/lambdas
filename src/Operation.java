public interface Operation {

    Integer operation(Integer a, Integer b);
}
